temperature = 70
forecast = "sunny"
raining = True

if temperature > 80 or temperature < 60 or raining: 
    print("Stay inside!")
elif temperature < 80 and not (forecast == "rain"): 
    print("Go outside!")
elif temperature < 60: 
    print("It's too hot!")
else: 
    print("Enjoy your outdoors!")
    
print("END")