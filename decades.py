age = int(input("How old?\n"))
decades = age // 10
years = age % 10

print("\nYou are " + str(decades) + " decades and " + str(years) + " years old.")

